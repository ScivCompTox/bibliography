


from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
#import reset

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^reset/', include('reset.urls')),
    url(r'', include('papers.urls', namespace='papers')),
        ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) +\
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

