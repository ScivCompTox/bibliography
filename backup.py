#! /usr/bin/env python


import os, shutil
from datetime import datetime

target = 'db.sqlite3'


file_name = datetime.strftime(datetime.now(),'%Y_%m_%d_%H_%M.sqlite3')

shutil.copyfile(target, file_name)



