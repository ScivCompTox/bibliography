from django.contrib import admin
from django.http import HttpResponseRedirect



from .models import Paper, Author, Affiliation

class AuthorInline(admin.TabularInline):
    model = Author.paper.through
    extra = 0
    
class AffiliationInline(admin.TabularInline):
    model = Affiliation.author.through
    extra = 0


class AuthorAdmin(admin.ModelAdmin):
    inlines = (AffiliationInline,
               )
    list_display = ('surname', 'forename', 'slug')
    search_fields = ('name', 'slug')


class PaperAdmin(admin.ModelAdmin):
    
    inlines = (AuthorInline,
               )
    def response_change(self, request, obj):
        if not '_continue' in request.POST:
            return HttpResponseRedirect('/')
        else:
            return super(PaperAdmin, self).response_change(request, obj)


    #TODO make this display the first author
    #TODO make the pub_date just show the date and not time
    list_display = ('pmid', 'title', 'journal', 'pub_date','modified_date')
    date_hierarchy = 'pub_date'
    #list_filter = ['pub_date', 'author']
    search_fields = ['title', 'pmid', 'journal']


    def response_change(self, request, obj):
        if not '_continue' in request.POST:
            print 'blah'
            return HttpResponseRedirect('/')
        else:
            return super(PaperAdmin, self).response_change(request, obj)


admin.site.register(Paper, 
                    PaperAdmin,)
                    
admin.site.register( Author, 
                    AuthorAdmin,)



from django.contrib import admin
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordResetForm,UserCreationForm
from django.utils.crypto import get_random_string

#from authtools.forms import UserCreationForm

User = get_user_model()


class UserCreationForm(UserCreationForm):
    """
    A UserCreationForm with optional password inputs.
    """

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['password1'].required = False
        self.fields['password2'].required = False
        # If one field gets autocompleted but not the other, our 'neither
        # password or both password' validation will be triggered.
        self.fields['password1'].widget.attrs['autocomplete'] = 'off'
        self.fields['password2'].widget.attrs['autocomplete'] = 'off'

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = super(UserCreationForm, self).clean_password2()
        if bool(password1) ^ bool(password2):
            raise forms.ValidationError("Fill out both fields")
        return password2


class UserAdmin(admin.ModelAdmin):
    #add_form = UserCreationForm
    #add_fieldsets = (
    #    (None, {
    #        'description': (
    #            "Enter the new user's name and email address and click save."
    #            " The user will be emailed a link allowing them to login to"
    #            " the site and set their password."
    #        ),
    #        'fields': ('email', 'name',),
    #    }),
    #    ('Password', {
    #        'description': "Optionally, you may set the user's password here.",
    #        'fields': ('password1', 'password2'),
    #        'classes': ('collapse', 'collapse-closed'),
    #    }),
    #)

    #def save_model(self, request, obj, form, change):
    #    if not change and not obj.has_usable_password():
    #        # Django's PasswordResetForm won't let us reset an unusable
    #        # password. We set it above super() so we don't have to save twice.
    #        obj.set_password(get_random_string())
    #        reset_password = True
    #    else:
    #        reset_password = False

    #    super(UserAdmin, self).save_model(request, obj, form, change)

    #    if reset_password:
    #        reset_form = PasswordResetForm({'email': obj.email})
    #        assert reset_form.is_valid()
    #        reset_form.save(
    #            subject_template_name='registration/account_creation_subject.txt',
    #            email_template_name='registration/account_creation_email.html',
    #        )

    pass
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
