from datetime import datetime

from django.shortcuts import render,get_object_or_404, render_to_response
from django.template import RequestContext
from django.views import generic
from django.conf import settings
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth.decorators import login_required

from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from .models import Paper, Author

from management.commands import add_by_pmid 


class IndexView(generic.ListView):
    template_name = 'papers/index.html'
    context_object_name = 'paper_list'
    paginate_by = 10
    
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(IndexView, self).get_context_data(**kwargs)
        # Add in the publisher
        context['featured_authors'] = Author.objects.filter(featured_author=True).order_by('surname')
        context['paper_years'] = [str(i) for i in reversed(sorted(set([p.pub_date.year for p in Paper.objects.only('pub_date')])))]
        context['current_query'] = self.request.GET
        if ('investigator' in self.request.GET): 
            context['selected_investigator'] = Author.objects.get(slug= self.request.GET['investigator'].strip())
        return context
    
    def get_queryset(self):
        entry_query=Paper.objects.all()
        if 'investigator' in self.kwargs:
            self.investigator = get_object_or_404(Author, slug=self.kwargs['investigator'])
            #filters['author'] = self.investigator
            entry_query = entry_query.filter(author=self.investigator)
            print len(entry_query)
        if ('investigator' in self.request.GET) and self.request.GET['investigator'].strip():
            entry_query = entry_query.filter(author__slug=self.request.GET['investigator'])
        if ('date' in self.request.GET) and self.request.GET['date'].strip():
            entry_query = entry_query.filter(pub_date__year=self.request.GET['date'])
        if ('q' in self.request.GET) and self.request.GET['q'].strip():
            self.query_string = self.request.GET['q']
            entry_query = entry_query.filter(get_query(self.query_string, ['title', 'journal', 'author__name'])).distinct()
        
        return entry_query.order_by('-pub_date')
        #return Paper.objects.filter(**filters).order_by('-pub_date')

class MissingPdfs(IndexView):
    
    def get_queryset(self):
        return [ paper for paper in Paper.objects.order_by('-pub_date') if bool(paper.pdf) == False]

#@login_required(login_url='/admin/login/')
@login_required()
def AddPmidForm(request):
   return render_to_response('papers/add_paper.html')

def SubmitPmid(request):
    pmid = request.GET['q']
    p = add_by_pmid.download_and_parse_pubmed(pmid)

    #return HttpResponse("Added PMID: {}".format(pmid))
    return HttpResponseRedirect(reverse('admin:papers_paper_change', args= (str(p.id),)))

        
        
        

#XXX Depricated
class QueryView(generic.ListView):
    template_name = 'papers/index.html'
    context_object_name = 'paper_list'
   
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(QueryView, self).get_context_data(**kwargs)
        # Add in the publisher
        context['investigator'] = self.investigator
        return context
   
    
    def get_queryset(self):
        self.investigator = get_object_or_404(Author, slug=self.kwargs['investigator'])
        return Paper.objects.filter(author=self.investigator).order_by('-pub_date')


class DetailView(QueryView):
    template_name = 'papers/index.html'
    context_object_name = 'paper_list'
   
    def get_context_data(self, **kwargs):
        context = super(QueryView, self).get_context_data(**kwargs)
        context['show_abstract'] = True
        return context
     
    def get_queryset(self):
        return Paper.objects.filter(slug=self.kwargs['slug'])

def DownloadPdf(request, slug):
    paper = get_object_or_404(Paper, slug=slug)
    
    # check to see if the paper exists
    if paper.pdf:
    
        # nationalize the name
        name = '{name}_{year}_{journal}_{pmid}'.format(**{'name':paper.author_set.all()[0].surname.lower(),
                          'journal':paper.journal_abbreviation.replace(' ','-').lower().replace('.',''),
                          'year':str(paper.pub_date.year),
                          'pmid':paper.pmid,})
        
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s.pdf"'%(name)
        response.write(file(paper.pdf.file.name, "rb").read())
        return response
    
    else:
        return HttpResponse('no pdf attached for this paper')
   

import re

from django.db.models import Q

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 



def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
    
    '''
    query = None # Query to search for every search term        
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query


class search(IndexView):
    template_name = 'papers/index.html'
    context_object_name = 'paper_list'

    def post(self, request):
        return self.get(request, self.get_queryset_post())

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(search, self).get_context_data(**kwargs)
        context['query_string'] = self.query_string
        return context


    def get_queryset_post(self):
        self.query_string = self.request.POST['q']
        
        entry_query = get_query(self.query_string, ['title', 'journal',
        'author__name'])
        
        return Paper.objects.filter(entry_query).order_by('-pub_date').distinct()

    def get_queryset(self):
        if ('q' in self.request.GET) and self.request.GET['q'].strip():
            self.query_string = self.request.GET['q']
        
        entry_query = get_query(self.query_string, ['title', 'journal', 'author__name'])
        
        return Paper.objects.filter(entry_query).order_by('-pub_date').distinct()
