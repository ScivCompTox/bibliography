from django.db import models



class Paper(models.Model):
    def __unicode__(self):
        return self.pmid
    title = models.CharField(max_length=1000)
    journal = models.CharField(max_length=1000)
    pub_date = models.DateTimeField()
    pmid = models.CharField(max_length=12, blank=True, null=True)
    abstract = models.TextField(default="", blank=True)
    volume = models.CharField(max_length=200, blank=True)
    issue = models.CharField(max_length=200, blank=True)
    start_page = models.CharField(max_length=200, blank=True)
    end_page = models.CharField(max_length=200, blank=True)
    journal_abbreviation = models.CharField(max_length=200)
    doi = models.CharField(max_length=200, blank=True)
    slug = models.SlugField()
    pdf = models.FileField(upload_to='publication_pdfs', blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
 


class Author(models.Model):
    def __unicode__(self):
        return self.name
    paper = models.ManyToManyField(Paper, through='AuthorOrder')
    name = models.CharField(max_length=200)
    surname = models.CharField(max_length=200)
    forename = models.CharField(max_length=200, blank=True, null=True)
    initials = models.CharField(max_length=200, blank=True, null=True)
    featured_author = models.BooleanField(default=False)
    slug = models.SlugField(default='1')
    
    
class Affiliation(models.Model):
    def __unicode__(self):
        return self.name
    author = models.ManyToManyField(Author)
    name = models.CharField(max_length=1000)


class AuthorOrder(models.Model):
    number = models.IntegerField()
    paper = models.ForeignKey(Paper)
    author = models.ForeignKey(Author)
