from django.core.urlresolvers import reverse
from django.contrib.auth.views import password_reset, password_reset_confirm

def reset_confirm(request, uidb36=None, token=None):
    return password_reset_confirm(request, template_name='../templates/registration/password_reset_confirm.html',
        uidb36=uidb36, token=token, post_reset_redirect=reverse('login'))


def reset(request):
    return password_reset(request, template_name='../templates/registration/password_reset_form.html',
        email_template_name='../templates/registration/password_reset_email.html',
        subject_template_name='../templates/registration/password_reset_subject.txt',
        post_reset_redirect=None)
