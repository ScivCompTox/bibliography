from django.conf.urls import url

from . import views



urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<slug>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<slug>[0-9]+)/pdf/$', views.DownloadPdf, name='download'),
    url(r'^author/(?P<investigator>[-\w]+)/$', views.IndexView.as_view(), name='query'),
    url(r'^date/(?P<date>[-\w]+)/$', views.IndexView.as_view(), name='query'),
    url(r'^query/$', views.IndexView.as_view(), name='search'),
    url(r'^papers-with-missing-pdfs/$', views.MissingPdfs.as_view()),
    url(r'^add-paper/$', views.AddPmidForm),
    url(r'^submit-pmid/$', views.SubmitPmid, name='submit-pmid'),
]
