# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('papers', '0002_remove_author_featured_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='author',
            name='featured_author',
            field=models.BooleanField(default=False),
        ),
    ]
