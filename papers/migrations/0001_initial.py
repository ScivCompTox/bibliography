# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Affiliation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=1000)),
            ],
        ),
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('surname', models.CharField(max_length=200)),
                ('forename', models.CharField(max_length=200, null=True, blank=True)),
                ('initials', models.CharField(max_length=200, null=True, blank=True)),
                ('featured_author', models.BooleanField(default=False)),
                ('slug', models.SlugField(default=b'1')),
            ],
        ),
        migrations.CreateModel(
            name='AuthorOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField()),
                ('author', models.ForeignKey(to='papers.Author')),
            ],
        ),
        migrations.CreateModel(
            name='Paper',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=1000)),
                ('journal', models.CharField(max_length=1000)),
                ('pub_date', models.DateTimeField()),
                ('pmid', models.CharField(max_length=12, null=True, blank=True)),
                ('abstract', models.TextField(default=b'', blank=True)),
                ('volume', models.CharField(max_length=200, blank=True)),
                ('issue', models.CharField(max_length=200, blank=True)),
                ('start_page', models.CharField(max_length=200, blank=True)),
                ('end_page', models.CharField(max_length=200, blank=True)),
                ('journal_abbreviation', models.CharField(max_length=200)),
                ('doi', models.CharField(max_length=200, blank=True)),
                ('slug', models.SlugField()),
                ('pdf', models.FileField(upload_to=b'publication_pdfs', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='authororder',
            name='paper',
            field=models.ForeignKey(to='papers.Paper'),
        ),
        migrations.AddField(
            model_name='author',
            name='paper',
            field=models.ManyToManyField(to='papers.Paper', through='papers.AuthorOrder'),
        ),
        migrations.AddField(
            model_name='affiliation',
            name='author',
            field=models.ManyToManyField(to='papers.Author'),
        ),
    ]
