from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

import requests
from populate_utilities import Article, parse

import sys
sys.path.append('../../../')
from papers.models import Paper, Author, Affiliation, AuthorOrder

def get_from_pubmed(pmid):
    base_url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=%s&retmode=xml'

    r = requests.get(base_url%pmid)
    return r.text


def download_and_parse_pubmed(pmid):
    ps = Paper.objects.filter(pmid=pmid)
    if ps:
        return ps[0]
    else:
        xml = get_from_pubmed(pmid)
        art = parse(xml)
        p = Paper(title=art.title,
                  journal=art.journal,
                  pub_date=art.date,
                  pmid=art.pmid,
                  volume=art.volume,
                  start_page=art.start_page,
                  end_page=art.end_page,
                  issue=art.issue,
                  abstract=art.abstract,
                  journal_abbreviation=art.journal_abbreviation,
                  slug=art.pmid,
                  )
        p.save()

        for index, author in enumerate(art.authors):

            #check if this author exists--create a new one if not
            query = Author.objects.filter(name=author[0])
            if query:
                a = query[0]
            else:
                a = Author(name = author[0],
                           forename = author[1],
                           surname = author[2],
                           initials = author[3],
                           slug = slugify(author[0]))
                a.save()
                
            # add paper-author many-to-many   
            author_order = AuthorOrder(number=index, paper=p, author=a)
            author_order.save()
            
             
            affiliations = author[4]
            for affiliation in affiliations:
                query = Affiliation.objects.filter(name=affiliation)
                if query:
                    query[0].author.add(a)
                else:
                    a.affiliation_set.create(name=affiliation)
            
        
        return p
        


class Command(BaseCommand):


    def add_arguments(self, parser):
        parser.add_argument('pmid', type=str)



    def handle(self, *args, **options):
        download_and_parse_pubmed(options['pmid'])

        
        
        
        
        
        
        
        

if __name__ == '__main__':
    xml = get_from_pubmed('25186463')
    art = parse(xml)
