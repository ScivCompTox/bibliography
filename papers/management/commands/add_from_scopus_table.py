
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

import requests
from populate_utilities import Article, parse

from papers.models import Paper, Author, Affiliation, AuthorOrder




def get_from_pubmed(pmid):
    base_url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=%s&retmode=xml'

    r = requests.get(base_url%pmid)
    return r.text

class Command(BaseCommand):


    def add_arguments(self, parser):
        parser.add_argument('records_file', type=str)


    def handle(self, *args, **options):
        with open(options['records_file']) as in_handle:

            data = {}
            for index, line in enumerate(in_handle):

                if not index:
                    header = dict([(j, field.strip()) for j, field in enumerate(line.strip().split('\t'))])
                else:
                    data[index] = dict([(header[j], field.strip()) for j, field in enumerate(line.strip().split('\t'))])

                    row = data[index]
                    pmid = row['PubMed ID']
                    if not pmid:
                        continue
                    ps = Paper.objects.filter(pmid=pmid)
                    if ps:
                        print ps
                        assert len(ps) == 1 
                        p = ps[0]
                        p.doi = row['DOI']
                        p.save()

                    else:
                        xml = get_from_pubmed(pmid)
                        try:
                            art = parse(xml)
                        except:
                            continue
                        p = Paper(title = row['Title'].strip('"'),
                                  journal = row['Source title'].strip('"'),
                                  journal_abbreviation = row['Abbreviated Source Title'],
                                  pmid=pmid,
                                  volume = row['Volume'],
                                  issue = row['Issue'],
                                  start_page= row['Page start'],
                                  end_page = row['Page end'],
                                  abstract = row['Abstract'].strip('"'),
                                  slug = pmid,
                                  pub_date = art.date,
                                  doi = row['DOI']
                                  )
                        p.save() 
                        print p.title
                        authors = row['Authors with affiliations'].strip('"').split(';')
                        for ai, author in enumerate(authors):
                            s = author.split()
                            surname = s[0].strip(', ')
                            initials= s[1].strip(', ').replace('.','')
                            affiliation = ' '.join(s[2:])

                            forename = ' '.join(initials)

                            full_name = forename + ' ' + surname
    
                            #check if this author exists--create a new one if not
                            query = Author.objects.filter(name=full_name)
                            if query:
                                a = query[0]
                            else:
                                a = Author(name = full_name,
                                           forename = forename, 
                                           surname = surname,
                                           initials = initials,
                                           slug = slugify(full_name))
                                a.save()
                                
                            # add paper-author many-to-many   
                            author_order = AuthorOrder(number=index, paper=p, author=a)
                            author_order.save()


                            query = Affiliation.objects.filter(name=affiliation)
                            if query:
                                query[0].author.add(a)
                            else:
                                a.affiliation_set.create(name=affiliation)
        
        


