from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

from papers.models import Paper, Author, Affiliation


def merge(f, t):
    from_author = Author.objects.filter(slug=f)
    if not from_author:
        return
    from_author = from_author[0]
        
    to_author = Author.objects.get(slug=t)

    for from_paper in Paper.objects.filter(author__slug=f):
        to_author.paper.add(from_paper)
        

    for affiliation in from_author.affiliation_set.all():
        to_author.affiliation_set.add(affiliation)
        
    from_author.affiliation_set.clear()
    from_author.delete()

class Command(BaseCommand):

    def handle(self, *args, **options):

        for a in Author.objects.all():
            
            s = a.slug
            q = Author.objects.filter(slug=s)
            if len(q) > 1:
                
                target = q[0]
                for i in q[1:]:
                    for p in Paper.objects.filter(author__pk=i.pk):
                        target.paper.add(p)
                    i.paper.clear()
                    i.delete()


