import random
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify
from difflib import SequenceMatcher
import requests
from populate_utilities import Article, parse

from datetime import datetime
from papers.models import Paper, Author, Affiliation




class Command(BaseCommand):


    def add_arguments(self, parser):
        parser.add_argument('records_file', type=str)


    def handle(self, *args, **options):
        with open(options['records_file']) as in_handle:

            data = {}
            for index, line in enumerate(in_handle):

                if not index:
                    header = dict([(j, field.strip()) for j, field in enumerate(line.strip().split('\t'))])
                else:
                    data[index] = dict([(header[j], field.strip()) for j, field in enumerate(line.strip().split('\t'))])

                    row = data[index]
                    pmid = row['PubMed ID']
                    best_score = 0.
                    best_entry = ''
                    if not pmid:
                        to_match = row['Title'].lower().strip('"')
                        for paper in Paper.objects.all():
                            title = paper.title.lower().encode('utf-8')
                            m = SequenceMatcher(None, title, to_match)
                            score = m.ratio()
                            if score > best_score:
                                best_score = score
                                best_entry = title



                        if best_score < 0.9:
                            pmid = ''.join(['0',]+[str(random.randrange(10)) for i in range(12)])
                            p = Paper(title = row['Title'].strip('"'),
                                      journal = row['Source title'].strip('"'),
                                      journal_abbreviation = row['Abbreviated Source Title'],
                                      pmid='',
                                      volume = row['Volume'],
                                      issue = row['Issue'],
                                      start_page= row['Page start'],
                                      end_page = row['Page end'],
                                      abstract = row['Abstract'].strip('"'),
                                      slug = pmid,
                                      pub_date = datetime(int(row['Year']),1,1),
                                      doi = row['DOI']
                                      )
                            p.save() 
                            authors = row['Authors with affiliations'].strip('"').split(';')
                            for ai, author in enumerate(authors):
                                s = author.split()
                                surname = s[0].strip(', ')
                                initials= s[1].strip(', ').replace('.','')
                                affiliation = ' '.join(s[2:])

                                forename = ' '.join(initials)

                                full_name = forename + ' ' + surname
        
                                #check if this author exists--create a new one if not
                                query = Author.objects.filter(name=full_name)
                                if query:
                                    a = query[0]
                                else:
                                    a = Author(name = full_name,
                                               forename = forename, 
                                               surname = surname,
                                               initials = initials,
                                               slug = slugify(full_name))
                                    a.save()
                                    
                                # add paper-author many-to-many   
                                a.paper.add(p)


                                query = Affiliation.objects.filter(name=affiliation)
                                if query:
                                    query[0].author.add(a)
                                else:
                                    a.affiliation_set.create(name=affiliation)
            
            
