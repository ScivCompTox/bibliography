
import codecs

from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

from papers.models import Paper, Author, Affiliation


class Command(BaseCommand):





    def handle(self, *args, **options):
        
        d = {}
        for a in Author.objects.all():
            if not a.surname in d:
                d[a.surname] = {}
            d[a.surname][a.slug] = a
        

        h = codecs.open('possible_duplicates.txt', 'wb', 'utf8')
        for name, slugs in d.items():
            if len(slugs) > 1:
                for a in sorted(slugs.values()):
                    if not a.forename:
                        a.forename = ''
                     
                    h.write('\t'.join([\
                                     str(len(a.paper.all())),
                                     (a.forename),
                                     a.surname, 
                                     a.slug, 
                                     ]))
                    h.write('\n')
                h.write('\n')
            
            
