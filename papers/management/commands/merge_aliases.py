from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

from papers.models import Paper, Author, Affiliation


def merge(f, t):
    from_author = Author.objects.filter(slug=f)
    if not from_author:
        return
    from_author = from_author[0]
        
    to_author = Author.objects.get(slug=t)

    for from_paper in Paper.objects.filter(author__slug=f):
        to_author.paper.add(from_paper)
        

    for affiliation in from_author.affiliation_set.all():
        to_author.affiliation_set.add(affiliation)
        
    from_author.affiliation_set.clear()
    from_author.delete()

class Command(BaseCommand):


    def add_arguments(self, parser):
        parser.add_argument('input', type=str)



    def handle(self, *args, **options):
        with open(options['input']) as in_handle:
            d = []
            for line in in_handle:
                field = line.strip()
                if not field:
                    for i in d[1:]:
                        to = d[0]
                        print i, to
                        merge(i, to)
                    d = []
                else:
                    d.append(field)

