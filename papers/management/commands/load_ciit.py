# -*- coding: utf-8 -*-
import os
import random
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify
from difflib import SequenceMatcher
from bs4 import BeautifulSoup
import requests
from populate_utilities import Article, parse

from datetime import datetime
from papers.models import Paper, Author, Affiliation, AuthorOrder


FIELDS = ['AUTHORS',
          'TITLE',
          'CITATION',
          "'PUBLICATION YEAR'",
          "'PUBLICATION TYPE'",
          'KEYWORDS',
          "'REPRINT NUMBER'",
          "'PDF IMAGE'",
          "'QA CHECK'",
          "'PDF Up-DATE'",
          "'PubMedAbstract'",
          "''DATE CREATED''"
]


def get_from_pubmed(pmid):
    base_url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=%s&retmode=xml'

    r = requests.get(base_url%pmid)
    return r.text

def abstract_url_to_pmid(title):

    base = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&retmode=xml&term=%s'

    return base%title

import codecs
class Command(BaseCommand):



    def handle(self, *args, **options):
        pmids = dict([[int(a) for a in line.strip().split()] for line in open('DatabasePopulation/pmids.txt')]) 
        records =[]
        with codecs.open('DatabasePopulation/Reprints.dmp', encoding='latin-1') as in_handle:
            record = {}
            for index, line in enumerate(in_handle):
                if line.startswith('$'):
                    records.append(record)
                    record = {}
                    continue

                is_start_field=False 
                for field in FIELDS:
                    if line.startswith(field):
                        is_start_field=True

                        if line.startswith("'PDF IMAGE'"):
                            pdf_file = line.replace("'PDF IMAGE' ", "").strip()
                            pdf_file = pdf_file.replace('http://libpc.ciit.org/','')
                            record['file'] = pdf_file
                            current_field = 'file'

                        elif line.startswith("TITLE"):
                            record['title'] = line.replace('TITLE ', '').strip()
                            current_field = 'title'
                        
                        elif line.startswith("AUTHORS"):
                            record['authors'] = line.replace('AUTHORS ', '').strip()
                            current_field = 'authors'
                        
                        elif line.startswith("'PUBLICATION YEAR'"):
                            record['year'] = line.replace("'PUBLICATION YEAR '", '').strip()
                            current_field = 'year'


                if not is_start_field:
                    record[current_field] += ' ' + line.strip()

                if index in pmids:
                    record['pmid'] = pmids[index]
        

        for record in records:
            if not 'title' in record:
                continue
            if 'pmid' in record:
                continue

            url = abstract_url_to_pmid(record['title'])
            r = requests.get(url)
            soup = BeautifulSoup(r.text, 'lxml')
            ids = soup.find_all('id')
            if len(ids) == 1:
                pmid = ids[0].text
                print record['title']
                if 'file' in record:
                    print '%s'%(ids[0].text), record['file']
                else:
                    print '%s'%(ids[0].text)

                if len(Paper.objects.filter(slug=pmid)):
                    p=Paper.objects.get(slug=pmid)
                    if not bool(p.pdf):
                        if 'file' in record:
                            p.pdf=record['file']
                            print p.pdf
                        p.save()
                    continue

                xml = get_from_pubmed(pmid)
                art = parse(xml)
                p = Paper(title=art.title,
                          journal=art.journal,
                          pub_date=art.date,
                          pmid=art.pmid,
                          volume=art.volume,
                          start_page=art.start_page,
                          end_page=art.end_page,
                          issue=art.issue,
                          abstract=art.abstract,
                          journal_abbreviation=art.journal_abbreviation,
                          slug=art.pmid,
                          )
                if 'file' in record:
                    p.pdf=record['file']
                p.save()

                for author in art.authors:

                    #check if this author exists--create a new one if not
                    query = Author.objects.filter(name=author[0])
                    if query:
                        a = query[0]
                    else:
                        a = Author(name = author[0],
                                   forename = author[1],
                                   surname = author[2],
                                   initials = author[3],
                                   slug = slugify(author[0]))
                        a.save()
                        
                    # add paper-author many-to-many   
                    author_order = AuthorOrder(number=index, paper=p, author=a)
                    author_order.save()
                    
                     
                    affiliations = author[4]
                    for affiliation in affiliations:
                        query = Affiliation.objects.filter(name=affiliation)
                        if query:
                            query[0].author.add(a)
                        else:
                            a.affiliation_set.create(name=affiliation)
                    
                
                
                
                
                
                
                
                
                
