from django.core.management.base import BaseCommand
from papers.models import Paper, Author, Affiliation


class Command(BaseCommand):
    
    def handle(self, *args, **options):

        for paper in Paper.objects.all():
            paper.delete()

        for author in Author.objects.all():
            author.delete()
        
        for affiliation in Affiliation.objects.all():
            affiliation.delete()
        
        
        
        
        
        
        
        
        
        
        
