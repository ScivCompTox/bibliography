from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify
from papers.models import Paper, Author, Affiliation

from populate_utilities import Article, parse

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'
    
    
    def add_arguments(self, parser):
        parser.add_argument('xml_file', type=file)

    def handle(self, *args, **options):
        art = parse(open(options['xml_file']))
        p = Paper(title=art.title,
                  journal=art.journal,
                  pub_date=art.date,
                  pmid=art.pmid,
                  volume=art.volume,
                  start_page=art.start_page,
                  end_page=art.end_page,
                  issue=art.issue,
                  abstract=art.abstract,
                  journal_abbreviation=art.journal_abbreviation,
                  slug=art.pmid,
                  )
        p.save()

        for author in art.authors:

            #check if this author exists--create a new one if not
            query = Author.objects.filter(name=author[0])
            if query:
                a = query[0]
            else:
                a = Author(name = author[0],
                           forename = author[1],
                           surname = author[2],
                           initials = author[3],
                           slug = slugify(author[0]))
                a.save()
                
            # add paper-author many-to-many   
            a.paper.add(p)
            
             
            affiliations = author[4]
            for affiliation in affiliations:
                query = Affiliation.objects.filter(name=affiliation)
                if query:
                    query[0].author.add(a)
                else:
                    a.affiliation_set.create(name=affiliation)
            
        
        
        
        
        
        
        
        
        
        
