
from django.core.management.base import BaseCommand
from django.template.defaultfilters import slugify

from papers.models import Paper, Author, Affiliation, AuthorOrder


class Command(BaseCommand):


    def add_arguments(self, parser):
        parser.add_argument('from', type=str)
        parser.add_argument('to', type=str)



    def handle(self, *args, **options):
        from_author = Author.objects.get(slug=options['from'])
        to_author = Author.objects.get(slug=options['to'])

        for from_authorpaper in AuthorOrder.objects.filter(author__slug=options['from']):
            # get the corresponding paper
            from_paper = from_authorpaper.paper
            
            author_order = AuthorOrder(number=from_authorpaper.number, paper=from_paper, author=to_author)
            author_order.save()

            from_authorpaper.delete()
            

        for affiliation in from_author.affiliation_set.all():
            to_author.affiliation_set.add(affiliation)
            
        from_author.affiliation_set.clear()
        from_author.delete()
