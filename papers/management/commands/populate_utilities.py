
from bs4 import BeautifulSoup
import datetime

class Article():
    
    def __init__(self, attrs={
                 "pmid":None,
                 "title":None,
                 "authors":None,
                 "date":None,
                 "journal":None,
                 "volume":None,
                 "start_page":None,
                 "end_page":None,
                 "url":None,
                 "abstract":None,
                 "issue":None,
                 }
                 ):

        for k, v in attrs.items():
            if v == None:
                setattr(self,k, "")
            else:
                setattr(self,k, v)

def parse(data):
    
    
    soup = BeautifulSoup(data, "lxml")
    
    obj = Article()
   
    medlinecite = soup.find_all('medlinecitation')[0]
    art = soup.find_all('medlinecitation')[0].find_all('article')[0]
    id_list = medlinecite.find_all('pmid')
    obj.pmid = id_list[0].text


    obj.title = art.find_all('articletitle')[0].text
    
    journal = art.find_all('journal')[0]
    
    
    obj.journal = journal.find_all('title')[0].text
    obj.journal_abbreviation = journal.find_all('isoabbreviation')[0].text

    if art.find_all("year"):
        year = art.find_all("year")[0].text
    else:
        year = art.find_all("medlinedate")[0].text.split()[0]
    if art.find_all("month"):
        month = art.find_all("month")[0].text
    else:
        month = '1' 
    if art.find_all("day"):
        day = art.find_all("day")[0].text
    else:
        day = '1'
   
    if not month.strip('.,').isdigit():
        if len(month.strip(',.')) == 3:
            month = datetime.datetime.strptime(month.strip(''), '%b').month
        else:
            month = datetime.datetime.strptime(month.strip(''), '%B').month
    obj.date = datetime.date(int(year), int(month), int(day))
   
    if art.find_all('abstracttext'):
        obj.abstract = art.find_all('abstracttext')[0].text
   
    authors = []
    for author in art.find_all('author'):
        forename = author.find_all('forename')
        surname = author.find_all('lastname')
        initials = author.find_all('initials')
        for name in [forename, surname, initials]:
            assert len(name) <= 1
        if forename: 
            forename=forename[0].text
        else: forename=''
        if surname: 
            surname=surname[0].text
        else: surname=''
         
        if initials: initials=initials[0].text
        else: initials=''
        authors.append(("%s %s"%(forename, surname),
                        forename,
                        surname,
                        initials,
                        [aff.text for aff in author.find_all('affiliation')],
                        ))
    
    obj.authors = authors
   
    if art.find_all("volume"):
        obj.volume = art.find_all("volume")[0].text
    if art.find_all("issue"):
        obj.issue = art.find_all("issue")[0].text
     
    if art.find_all('startpage'):
        obj.start_page = art.find_all('startpage')[0].text
    
        if art.find_all('endpage'):
            obj.end_page = art.find_all('endpage')[0].text
    else:
        if art.find_all('medlinepgn'):
            obj.start_page = art.find_all('medlinepgn')[0].text
   
    return obj


