# Import the reverse shortcut to get the URLs of URL patterns using just their name.
from django.core.urlresolvers import reverse

# Import the built-in password reset view and password reset confirmation view.
from django.contrib.auth.views import password_reset, password_reset_confirm
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth import get_user_model

from django.http import HttpResponse


# Import the render shortcut to render the templates in response.
from django.shortcuts import render

# This view handles the password reset form URL /.
def reset(request):
    # Wrap the built-in password reset view and pass it the arguments
    # like the template name, email template name, subject template name
    # and the url to redirect after the password reset is initiated.
    return password_reset(request, template_name='reset.html',
        email_template_name='reset_email.html',
        subject_template_name='reset_subject.txt',
        post_reset_redirect = reverse('password_reset_done'),
        password_reset_form=PasswordResetorCreateUser)

def reset_done(request):
  return render(request, "link_sent.html")

# This view handles password reset confirmation links. See urls.py file for the mapping.
# This view is not used here because the password reset emails with confirmation links
# cannot be sent from this application.
def reset_confirm(request, uidb64=None, token=None):
    # Wrap the built-in reset confirmation view and pass to it all the captured parameters like uidb64, token
    # and template name, url to redirect after password reset is confirmed.
    return password_reset_confirm(request, template_name='password_reset_confirm.html',
        uidb64=uidb64, token=token, post_reset_redirect=reverse('success'))

# This view renders a page with success message.
def success(request):
  return render(request, "success.html")



class PasswordResetorCreateUser(PasswordResetForm):


    def get_users(self, email):
        active_users = get_user_model()._default_manager.filter(
            email__iexact=email, is_active=True)
   
        # make a user if one doesn't exist     
        if not active_users:
            user_model = get_user_model() 
            password = user_model.objects.make_random_password()
            user = user_model.objects.create_user(email, email, password)
            user.is_staff = True
            user.is_superuser = True
            user.save()
            
            active_users = [user,]
        return active_users
