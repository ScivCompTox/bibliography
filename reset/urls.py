
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [ url(r'^$', 'reset.views.reset', name='reset'),
                url(r'^ba$', 'reset.views.reset_done', name='password_reset_done'),
                url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 'reset.views.reset_confirm', name='password_reset_confirm'),
                url(r'^success/$', 'reset.views.success', name='success'),
               ]
